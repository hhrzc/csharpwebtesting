﻿using System;
using ConsoleApp1.Elements;
using OpenQA.Selenium;
using OpenQA.Selenium.DevTools.Network;

namespace ConsoleApp1.Pages
{
    public class LoginPage
    {
        // protected static IWebDriver WebDriver = Driver.CreateDriver(Driver.Browser.Chrome);

        private WDButton _login;
        public WDButton Login
        {
            get
            {
                if (_login == null)
                {
                    _login = new WDButton(By.Id("qode_social_icon_widget-4"), "Login Button");
                }

                return _login;
            }
        } 

        
    }
}