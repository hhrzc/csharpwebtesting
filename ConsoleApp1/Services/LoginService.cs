﻿using System;
using ConsoleApp1.Elements;
using ConsoleApp1.Pages;

namespace ConsoleApp1.Services
{
    public class LoginService
    {
        private LoginPage _loginPage = new LoginPage();

        
        public void Login()
        {
            //....
            Element.Log += PrintLogInConsole;
            _loginPage.Login.Click();
            Browser.WaitAjax();
            //....
        }

        private void PrintLogInConsole(string message)
        {
            Console.WriteLine(message);
        }
    }
}