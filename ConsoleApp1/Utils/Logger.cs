﻿using System;


namespace ConsoleApp1
{
    internal interface Logger
    {
        internal delegate void ReportWriter(string report);
        internal event ReportWriter Log;

        public static void PrintConsole(string message)
        {
            Console.WriteLine(message);
        }
    }
}