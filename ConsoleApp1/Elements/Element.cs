﻿using System;
using OpenQA.Selenium;
using static ConsoleApp1.Logger;

namespace ConsoleApp1.Elements
{
    public abstract class Element
    {
        protected static IWebDriver WebDriver = Browser.GetWebDriver();
        public readonly IWebElement WebElement;
        public readonly string Name;

        public Element(IWebElement element, string name = "")
        {
            if (name.Length == 0)
            {
                name = "Element with tag = " + element.TagName;
            }

            Name = name;
            WebElement = element;
        }

        public Element(By selector, string name = "")
        {
            //todo: test
            if (name.Length == 0)
            {
                name = "Element with " + selector.Mechanism + " = " + selector.Criteria;
            }
            
            
        }
        
        //todo: add log level (success, error, etc...)
        // possible bad way - need to analyze
        internal delegate void ReportWriter(string report);
        internal static event ReportWriter Log;
        
        public void Click()
        {
            try
            {
                WebElement.Click();
                
                Log?.Invoke("The " + Name + " is clicked");
            }
            catch (Exception e)
            {
                Log?.Invoke("The " + Name + " isn't clicked");
                Log?.Invoke(e.Message);
                Log?.Invoke(e.StackTrace);
            }
        }
    }
}