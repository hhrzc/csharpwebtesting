﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace ConsoleApp1.Elements
{
    public class WDButton : Element
    {

        // public WDButton(By selector, string name) : base()
        // {
        //     IWebElement wl = Browser.FindElement(selector);
        // }

        public void Clear()
        {
            WebElement.Clear();
            Log += delegate(string report) {  };
            
        }

        public WDButton(IWebElement element, string name = "") : base(element, name)
        {
        }

        public WDButton(By selector, string name = "") : base(selector, name)
        {
        }
    }
}