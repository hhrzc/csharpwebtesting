﻿using System;
using System.Threading;
using ConsoleApp1.Pages;
using ConsoleApp1.Services;

namespace ConsoleApp1
{
    public class MainClass
    {
        public static void Main(string[] args)
        {
            Browser.Start();
            Browser.Navigate(new Uri("https://qatestlab.com"));
            // Thread.Sleep(System.TimeSpan.FromMilliseconds(7000));
            new LoginService().Login();
            Thread.Sleep(System.TimeSpan.FromMilliseconds(5000));
            Browser.Quit();
        }
    }
}